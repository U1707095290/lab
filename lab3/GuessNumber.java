import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {	
		Scanner reader = new Scanner(System.in); //Creates an object to read user input
		Random rand = new Random(); //Creates an object from Random class
		int tries=0;
		int number =rand.nextInt(100); //generates a number between 0 and 99
		System.out.println("Hi! I'm thinking of a number between 0 and 99.");
		System.out.print("Can you guess it: ");



        while(true){
			tries=tries+1;
			int guess = reader.nextInt(); //Read the user input
			if (guess==number){
				System.out.println("Congradulations! You have won after "+tries + " tries.");
				break;
			}else if(guess==-1){
				System.out.println("Sorry the number was "+number);
				break;
			}
			else{
			    if(number < guess)
					System.out.print("Mine is less than your guess\n");
				else
					System.out.print("Mine is greater than your guess\n");
					
				System.out.print("Type -1 to quit or guess another: ");
			}
		}
		reader.close(); //Close the resource before exiting
	}
	
	
}
