class FindPrimes{
    public static void main(String[] args){
        int num=Integer.valueOf(args[0]);
    
        for(int i=2; i<=num; i=i+1){
            boolean prime=true;
            for(int n=2; n<i; n++){
                if(i % n==0)
                    prime=false;
            }           
            if(prime)
                System.out.print(i+",");
        }
        System.out.println();
    }
}